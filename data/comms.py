
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtCore import pyqtSignal, QObject, QTimer

from data.util import pop_all, rounded
from data.utc import now

from session.config import settings


# ---------- Constants ----------

# -------------------------------


# Pronunciation dictionaries: code -> (TTS str, SR phoneme list)
phon_airlines = {}
phon_navpoints = {}


def get_TTS_string(dct, key):
	return dct[key][0]

def get_phonemes(dct, key):
	return dct[key][1]



class ChatMessage:
	def __init__(self, sender, text, recipient=None, private=False):
		self.sent_by = sender
		self.known_recipient = recipient
		self.text = text
		self.private = private
		if recipient == None and private:
			self.private = False
			print('WARNING: Cannot make message private without an identified recipient; made public.')
		self.msg_time_stamp = now()

	def txtOnly(self):
		return self.text

	def txtMsg(self):
		if self.known_recipient == None or self.known_recipient == '' or self.private:
			return self.text
		else:
			return '%s: %s' % (self.known_recipient, self.text)
	
	def isPrivate(self):
		return self.private

	def sender(self):
		return self.sent_by

	def recipient(self):
		return self.known_recipient
	
	def timeStamp(self):
		return self.msg_time_stamp
	
	def isFromMe(self):
		return self.sender() == settings.session_manager.myCallsign()
	
	def involves(self, name):
		return self.sender() == name or self.recipient() == name







class CommFrequency:
	spacing = .025 / 3 # 8.33 kHz
	
	def __init__(self, mhz):
		if isinstance(mhz, str):
			if '.' in mhz:
				dec_part = mhz.rsplit('.', maxsplit=1)[-1]
				if len(dec_part) == 2 and dec_part[-1] in '27': # ._2 and ._7 endings are shortened 25kHz-step freq's
					mhz += '5'
			self.mhz = rounded(float(mhz), CommFrequency.spacing)
			if abs(self.mhz) < 1e-6: # freq not even 1 Hz
				raise ValueError('invalid near-zero comm frequency')
		else:
			self.mhz = mhz
	
	def __str__(self):
		return '%0.3f' % self.mhz
	
	def MHz(self):
		return self.mhz
	
	def inTune(self, other):
		return abs(self.mhz - other.mhz) < CommFrequency.spacing / 2





class RadioDirectionFinder(QObject):
	signalChanged = pyqtSignal()
	
	def __init__(self, position_coords):
		QObject.__init__(self)
		self.coordinates = position_coords
		self.received_signals = [] # (key, get signal origin function) pairs
	
	def currentSignalRadial(self):
		try:
			return self.coordinates.headingTo(self.received_signals[0][1]())
		except IndexError:
			return None
	
	def receivingSignal(self, signal_key):
		return any(k == signal_key for k, f in self.received_signals)
	
	def receiveSignal(self, signal_key, fGetOrigin, timeOut=None):
		if settings.radio_direction_finding:
			self.received_signals.insert(0, (signal_key, fGetOrigin))
			if timeOut != None:
				QTimer.singleShot(1000 * timeOut.total_seconds(), lambda sig=signal_key: self.dieSignal(sig))
			self.signalChanged.emit()
	
	def dieSignal(self, signal_key):
		pop_all(self.received_signals, lambda sig: sig[0] == signal_key)
		self.signalChanged.emit()
	
	def clearAllSignals(self):
		self.received_signals.clear()
		self.signalChanged.emit()









class CpdlcMessage:
	# STATIC
	types = REQUEST, INSTR, FREE_TEXT, ACK, REJECT = range(5)
	
	def type2str(msg_type):
		return {
				CpdlcMessage.REQUEST: 'REQUEST',
				CpdlcMessage.INSTR: 'INSTR',
				CpdlcMessage.FREE_TEXT: 'TEXT',
				CpdlcMessage.ACK: 'ACK',
				CpdlcMessage.REJECT: 'REJECT'
			}[msg_type]
	
	def fromText(from_me, text):
		sep = text.split(' ', maxsplit=1) # len(sep) is 1 or 2
		try:
			msg_type = next(t for t in CpdlcMessage.types if CpdlcMessage.type2str(t) == sep[0])
			return CpdlcMessage(from_me, msg_type, contents=(None if len(sep) == 1 else sep[1]))
		except StopIteration: # Fallback on free text if type is not recognised
			return CpdlcMessage(from_me, CpdlcMessage.FREE_TEXT, contents=text)
	
	# OBJECT METHODS
	def __init__(self, from_me, msg_type, contents=None):
		self.from_me = from_me  # True = sent by me to them; False otherwise
		self.msg_type = msg_type
		self.msg_contents = contents
		self.time_stamp = now()
	
	def isFromMe(self):
		return self.from_me
	
	def type(self):
		return self.msg_type
	
	def contents(self):
		return self.msg_contents
	
	def text(self):
		res = CpdlcMessage.type2str(self.msg_type)
		if self.msg_contents != None:
			res += ' ' + self.msg_contents
		return res
	
	def timeStamp(self):
		return self.time_stamp
