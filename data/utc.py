
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from datetime import datetime, timezone, timedelta


# ---------- Constants ----------

# -------------------------------




def now():
	return datetime.now(timezone.utc)


def timestr(t=None, seconds=False):
	if t == None:
		t = now()
	res = '%02d:%02d' % (t.hour, t.minute)
	if seconds:
		res += ':%02d' % t.second
	return res

def datestr(t=None, year=True):
	if t == None:
		t = now()
	res = '%d/%02d' % (t.day, t.month)
	if year:
		res += '/%d' % t.year
	return res

def rel_datetime_str(dt):
	d = dt.date()
	return ('today' if d == now().date() else datestr(d, year=False)) + ' at ' + timestr(dt.time())




def duration_str(td):
	seconds = int(round(td.total_seconds()))
	hours = seconds // 3600
	if hours == 0:
		return '%d min %02d s' % (seconds // 60, seconds % 60)
	else:
		return '%d h %02d min' % (hours, seconds % 3600 // 60)


last_stopwatch_reset = now()

def reset_stopwatch():
	global last_stopwatch_reset
	last_stopwatch_reset = now()


def read_stopwatch():
	'''
	returns a timedelta
	'''
	return (now() - last_stopwatch_reset).total_seconds()



